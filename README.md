# Catalogue to Laravel 5.2

## Installation

Before install animaldevs/catalogue, you need install animaldevs/sergeant to load application base

**1 - After install Laravel framework, insert on file composer.json, inside require object this value**
```
"animaldevs/catalogue": "~1.0"
```

and execute on console:
```
composer install
```

**2 - Register service provider, on file config/app.php add to providers array**

```
Rent\Catalogue\CatalogueServiceProvider::class,

```

**3 - To publish package and migrate**

and execute composer update again:
```
composer update
```

**4 - Run seed database**

```
php artisan db:seed --class="CatalogueTableSeeder"
```

**5 - Activate package**

Access to Sergeant Panel, and go to Administration -> Permissions -> Profiles, and set all permissions to your profile by clicking on the open lock.

**6 - to use auth properties, include this arrays in config/auth.php**

Inside guards array
```
'catalogue' => [
    'driver'    => 'session',
    'provider'  => 'catalogueCustomer',
],
```

Inside providers array
```
'catalogueCustomer' => [
    'driver'    => 'eloquent',
    'model'     => Rent\Catalogue\Models\Customer::class,
],
```

Inside passwords array
```
'cataloguePasswordBroker' => [
    'provider'  => 'catalogueCustomer',
    'email'     => 'sergeant::emails.password',
    'table'     => '001_021_password_resets',
    'expire'    => 60,
],
```

you can change email cataloguePasswordBroker, to custom appearance.

**7 - How get auth properties**
Use catalogue guard to get auth properties
```
auth('catalogue')
```