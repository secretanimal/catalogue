<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CatalogueTableSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();

        $this->call(CataloguePackageTableSeeder::class);
        $this->call(CatalogueResourceTableSeeder::class);
        $this->call(CatalogueAttachmentMimeSeeder::class);

        Model::reguard();
    }
}

/*
 * Command to run:
 * php artisan db:seed --class="CatalogueTableSeeder"
 */