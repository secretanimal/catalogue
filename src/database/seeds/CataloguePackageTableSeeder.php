<?php

use Illuminate\Database\Seeder;
use Rent\Sergeant\Models\Package;

class CataloguePackageTableSeeder extends Seeder
{
    public function run()
    {
        Package::insert([
            ['id_012' => '9', 'name_012' => 'Catalogue Package', 'folder_012' => 'catalogue', 'sorting_012' => 9, 'active_012' => false]
        ]);
    }
}

/*
 * Command to run:
 * php artisan db:seed --class="CataloguePackageTableSeeder"
 */