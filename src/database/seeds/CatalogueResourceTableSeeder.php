<?php

use Illuminate\Database\Seeder;
use Rent\Sergeant\Models\Resource;

class CatalogueResourceTableSeeder extends Seeder {

    public function run()
    {
        Resource::insert([
            ['id_007' => 'catalogue',             'name_007' => 'Catalogue Package',    'package_id_007' => '9'],
            ['id_007' => 'catalogue-customer',    'name_007' => 'Customers',      'package_id_007' => '9'],
            ['id_007' => 'catalogue-group',       'name_007' => 'Groups',         'package_id_007' => '9'],
        ]);
    }
}

/*
 * Command to run:
 * php artisan db:seed --class="CatalogueResourceTableSeeder"
 */