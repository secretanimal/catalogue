<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can any all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['middleware' => ['web', 'sergeant']], function() {
    
/*
|--------------------------------------------------------------------------
| CUSTOMER
|--------------------------------------------------------------------------
*/
    Route::any(config('sergeant.appName') . '/catalogue/customers/{modal}/{offset?}',                   ['as'=>'catalogueCustomer',                   'uses'=>'Rent\Catalogue\Controllers\CustomerController@index',                      'resource' => 'catalogue-customer',        'action' => 'access']);
    Route::any(config('sergeant.appName') . '/catalogue/customers/json/data/{modal}',                   ['as'=>'jsonDataCatalogueCustomer',           'uses'=>'Rent\Catalogue\Controllers\CustomerController@jsonData',                   'resource' => 'catalogue-customer',        'action' => 'access']);
    Route::get(config('sergeant.appName') . '/catalogue/customers/create/{offset}/{tab}/{modal}',       ['as'=>'createCatalogueCustomer',             'uses'=>'Rent\Catalogue\Controllers\CustomerController@createRecord',               'resource' => 'catalogue-customer',        'action' => 'create']);
    Route::post(config('sergeant.appName') . '/catalogue/customers/store/{offset}/{tab}/{modal}',       ['as'=>'storeCatalogueCustomer',              'uses'=>'Rent\Catalogue\Controllers\CustomerController@storeRecord',                'resource' => 'catalogue-customer',        'action' => 'create']);
    Route::get(config('sergeant.appName') . '/catalogue/customers/{id}/edit/{offset}/{tab}/{modal}',    ['as'=>'editCatalogueCustomer',               'uses'=>'Rent\Catalogue\Controllers\CustomerController@editRecord',                 'resource' => 'catalogue-customer',        'action' => 'access']);
    Route::put(config('sergeant.appName') . '/catalogue/customers/update/{id}/{offset}/{tab}/{modal}',  ['as'=>'updateCatalogueCustomer',             'uses'=>'Rent\Catalogue\Controllers\CustomerController@updateRecord',               'resource' => 'catalogue-customer',        'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/catalogue/customers/delete/{id}/{offset}/{modal}',        ['as'=>'deleteCatalogueCustomer',             'uses'=>'Rent\Catalogue\Controllers\CustomerController@deleteRecord',               'resource' => 'catalogue-customer',        'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/catalogue/customers/delete/select/records/{modal}',    ['as'=>'deleteSelectCatalogueCustomer',       'uses'=>'Rent\Catalogue\Controllers\CustomerController@deleteRecordsSelect',        'resource' => 'catalogue-customer',        'action' => 'delete']);


    /*
    |--------------------------------------------------------------------------
    | GROUP
    |--------------------------------------------------------------------------
    */
    Route::any(config('sergeant.appName') . '/catalogue/groups/{offset?}',                          ['as'=>'catalogueGroup',                   'uses'=>'Rent\Catalogue\Controllers\GroupController@index',                      'resource' => 'catalogue-group',        'action' => 'access']);
    Route::any(config('sergeant.appName') . '/catalogue/groups/json/data',                          ['as'=>'jsonDataCatalogueGroup',           'uses'=>'Rent\Catalogue\Controllers\GroupController@jsonData',                   'resource' => 'catalogue-group',        'action' => 'access']);
    Route::get(config('sergeant.appName') . '/catalogue/groups/create/{offset}',                    ['as'=>'createCatalogueGroup',             'uses'=>'Rent\Catalogue\Controllers\GroupController@createRecord',               'resource' => 'catalogue-group',        'action' => 'create']);
    Route::post(config('sergeant.appName') . '/catalogue/groups/store/{offset}',                    ['as'=>'storeCatalogueGroup',              'uses'=>'Rent\Catalogue\Controllers\GroupController@storeRecord',                'resource' => 'catalogue-group',        'action' => 'create']);
    Route::get(config('sergeant.appName') . '/catalogue/groups/{id}/edit/{offset}',                 ['as'=>'editCatalogueGroup',               'uses'=>'Rent\Catalogue\Controllers\GroupController@editRecord',                 'resource' => 'catalogue-group',        'action' => 'access']);
    Route::put(config('sergeant.appName') . '/catalogue/groups/update/{id}/{offset}',               ['as'=>'updateCatalogueGroup',             'uses'=>'Rent\Catalogue\Controllers\GroupController@updateRecord',               'resource' => 'catalogue-group',        'action' => 'edit']);
    Route::get(config('sergeant.appName') . '/catalogue/groups/delete/{id}/{offset}',               ['as'=>'deleteCatalogueGroup',             'uses'=>'Rent\Catalogue\Controllers\GroupController@deleteRecord',               'resource' => 'catalogue-group',        'action' => 'delete']);
    Route::delete(config('sergeant.appName') . '/catalogue/groups/delete/select/records',           ['as'=>'deleteSelectCatalogueGroup',       'uses'=>'Rent\Catalogue\Controllers\GroupController@deleteRecordsSelect',        'resource' => 'catalogue-group',        'action' => 'delete']);

});