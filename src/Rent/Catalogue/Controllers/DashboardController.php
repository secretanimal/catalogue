<?php namespace Rent\Catalogue\Controllers;

use Rent\Sergeant\Core\Controller;

/**
 * Class DashboardController
 * @package Rent\Sergeant\Controllers
 */

class DashboardController extends Controller
{
    protected $folder       = 'dashboard';
    protected $package      = 'catalogue';
    
    public function index()
    {
        $data['package']        = $this->package;
        $data['folder']         = $this->folder;

        return view('catalogue::dashboard.index', $data);
    }
}