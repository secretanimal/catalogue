<?php namespace Rent\Catalogue\Controllers;

use Rent\Sergeant\Core\Controller;
use Rent\Catalogue\Models\Group;

/**
 * Class GroupController
 * @package Rent\Catalogue\Controllers
 */

class GroupController extends Controller
{
    protected $routeSuffix  = 'catalogueGroup';
    protected $folder       = 'group';
    protected $package      = 'catalogue';
    protected $indexColumns = ['id_300', 'name_300'];
    protected $nameM        = 'name_300';
    protected $model        = Group::class;
    protected $icon         = 'fa fa-users';
    protected $objectTrans  = 'group';

    public function storeCustomRecord($parameters)
    {
        Group::create([
            'name_300'  => $this->request->input('name')
        ]);
    }
    
    public function updateCustomRecord($parameters)
    {
        Group::where('id_300', $parameters['id'])->update([
            'name_300'  => $this->request->input('name')
        ]);
    }
}