<?php namespace Rent\Catalogue;

use Illuminate\Support\ServiceProvider;

class CatalogueServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		// include route.php file
		if (!$this->app->routesAreCached())
			require __DIR__ . '/../../routes.php';

		// register views
		$this->loadViewsFrom(__DIR__ . '/../../views', 'catalogue');

        // register translations
        $this->loadTranslationsFrom(__DIR__ . '/../../lang', 'catalogue');

		// register public files
		$this->publishes([
			__DIR__ . '/../../../public' 	=> public_path('/packages/rent/catalogue')
		]);

		// register config files
		$this->publishes([
			__DIR__ . '/../../config/catalogue.php' 				=> config_path('catalogue.php')
		]);

        // register migrations
        $this->publishes([
            __DIR__ . '/../../database/migrations/' 		=> base_path('/database/migrations'),
			__DIR__ . '/../../database/migrations/updates/' => base_path('/database/migrations/updates'),
        ], 'migrations');

        // register migrations
        $this->publishes([
            __DIR__ . '/../../database/seeds/' 				=> base_path('/database/seeds')
        ], 'seeds');
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
        //
	}
}