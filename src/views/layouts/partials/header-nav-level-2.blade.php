<header>
		<nav class="navbar navbar-default">
			<div class="container-fluid">				    

				<div class="navbar-header">				
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="template_dev.html">
						<img src="img/logo_small.png" class="logo_small">
					</a>				
				</div>

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">								
						<li>
							<a href="javascript:;" class="rs_btn dashboard">
								<span class="icons icon-dashboard" aria-hidden="true"></span>
								DASHBOARD
							</a>
						</li>
						<li class="active">
							<a href="template_catalogos.html" class="rs_btn active-catalogos">
								<span class="icons icon-catalogos icon-active-catalogos" aria-hidden="true"></span>
								CATÁLOGOS
							</a>
						</li>
						<li>							
							<a href="javascript:;" class="rs_btn reportes">
								<span class="icons icon-reportes" aria-hidden="true"></span>
								REPORTES
							</a>
						</li>
						<li>
							<a href="javascript:;" class="rs_btn settings">
								<span class="icons icon-settings" aria-hidden="true"></span>
								SETTINGS
							</a>
						</li>				       
					</ul>
				      
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a id="rs_notifications" href="#" class="dropdown-toggle rs_btn2" data-toggle="dropdown">
								<span class="glyphicon glyphicon-bell" aria-hidden="true">
									<span class="badge badge-notify">4</span>
								</span>
							</a>
																            
							<ul class="dropdown-menu notifications" role="menu" aria-labelledby="rs_notifications">
								<div class="notification-heading">
									<h4 class="menu-title title-notifications">Notificaciones</h4>
									<h4 class="menu-title pull-right">Ver todas<i class="glyphicon glyphicon-circle-arrow-right"></i></h4>
								</div>

								<li class="divider notification-divider"></li>

								<div class="notifications-wrapper">									    
									<a class="content" href="#">
										<div class="notification-item">
											<h4 class="item-title">Notificación de Cobro</h4>
											<p class="item-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
										</div>									      
									</a>

									<a class="content" href="#">
										<div class="notification-item">
											<h4 class="item-title">Notificación de Pago</h4>
											<p class="item-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
										</div>									      
									</a>

									<a class="content" href="#">
										<div class="notification-item">
											<h4 class="item-title">Notificación de Contrato</h4>
											<p class="item-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
										</div>									      
									</a>

									<a class="content" href="#">
										<div class="notification-item">
											<h4 class="item-title">Notificación de Cobro</h4>
											<p class="item-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
										</div>									      
									</a>
								</div>
									
								<li class="divider"></li>
									
								<div class="notification-footer">
									<h4 class="menu-title">Cerrar<i class="glyphicon glyphicon-remove-sign"></i></h4>
								</div>
							</ul>
						</li>
						
						<li>
							<a id="rs_panel_user" href="#" class="dropdown-toggle profile-image" data-toggle="dropdown">
								<img src="img/user.png" class="img-circle pic-user-small"> 
								User <b class="caret"></b>
							</a>

							<ul class="dropdown-menu panel-user" role="menu" aria-labelledby="rs_panel_user">

								<div class="panel-user-heading">
									<img src="img/user.png" class="img-circle pic-user-regular">
											
									<div class="panel-user-opcion user-info">
										<h4 class="panel-user-menu-title user-name">
										Nombre Apellido
										</h4>
										<h5 class="panel-user-opcion user-mail">
										ejemplo@ejemplo.com
										</h5>
										<h4 class="panel-user-opcion user-change-info">
											<a href="#">Cambiar mi información</a>
										</h4>
									</div>
								</div>

								<li class="divider panel-user-divider"></li>

								<div class="panel-user-wrapper">								   		

									<div class="panel-user-plazas">
										<h4 class="panel-user-menu-title">Mis plazas</h4>
									</div>
									
									<div class="panel-user-item">
										<div class="row plaza-box-margin-r">
											<div class="col-lg-2 col-md-2 col-sm-2 img-user-large">
												<img src="img/user.png" class="img-circle special-img pic-user-large">
											</div>
											
											<div class="col-lg-8 col-md-8 col-sm-8">
												<h4 class="panel-user-menu-title">Nombre de la Plaza</h4>
												<h5 class="panel-user-opcion plaza-box">
													<span class="label label-success">Actual</span>
												</h5>

											</div>
										</div>
									</div>									      
									
									<a class="content" href="#">
									<div class="panel-user-item">
										<div class="row plaza-box-margin-r">
											<div class="col-lg-2 col-md-2 col-sm-2 img-user-large">
												<img src="img/user.png" class="img-circle special-img pic-user-large">
											</div>
											
											<div class="col-lg-8 col-md-8 col-sm-8">
												<h4 class="panel-user-menu-title">Nombre de la Plaza</h4>
												<h5 class="panel-user-opcion plaza-box">
													<button class="btn btn-primary">Seleccionar</button>
												</h5>

											</div>
										</div>
									</div>
									</a>

									<a class="content" href="#">
									<div class="panel-user-item">
										<div class="row plaza-box-margin-r">
											<div class="col-lg-2 col-md-2 col-sm-2 img-user-large">
												<img src="img/user.png" class="img-circle special-img pic-user-large">
											</div>
											
											<div class="col-lg-8 col-md-8 col-sm-8">
												<h4 class="panel-user-menu-title">Nombre de la Plaza</h4>
												<h5 class="panel-user-opcion plaza-box">
													<button class="btn btn-primary">Seleccionar</button>
												</h5>

											</div>
										</div>
									</div>
									</a>									
								</div>
									    
								<li class="divider"></li>
									    
								<div class="panel-user-footer">
									<h4 class="panel-user-menu-title">Cerrar<i class="glyphicon glyphicon-remove-sign"></i></h4>
								</div>

							</ul>
						   
						</li>				        
						        
					</ul>
				</div>
			</div>
		</nav>

		<nav class="navbar navbar-default" style="z-index: 1;">
			<div class="container-fluid">
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="padding-left: 95px;">
					<ul class="nav navbar-nav">								
						<li>							
							<a href="javascript:;" class="rs_btn submenu-active"><span style="padding-left: 15px; ">Inmuebles</span></a>
						</li>						
						<li style="">							
							<a href="javascript:;" class="rs_btn submenu"><span style="padding-left: 15px; ">Clientes</span></a>
						</li>
						<li style="">							
							<a href="javascript:;" class="rs_btn submenu"><span style="padding-left: 15px; ">Contratos</span></a>
						</li>
						<li style="">
							<a href="javascript:;" class="rs_btn submenu"><span style="padding-left: 15px; ">Usuarios</span></a>
						</li>
					</ul>
				      
				    <button class="badge rs-new">+</button>
				</div>
			</div>
		</nav>
    </header>