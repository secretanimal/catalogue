<style type="text/css">

	.dropdown-menu {
	    position: absolute;
	    top: 100%;
	    left: 0;
	    z-index: 1000;
	    display: none;
	    float: left;
	    min-width: 160px;
	    padding: 5px 0;
	    margin: 2px 0 0;
	    font-size: 14px;
	    text-align: left;
	    list-style: none;
	    background-color: #fff;
	    -webkit-background-clip: padding-box;
	    background-clip: padding-box;
	    border: 0px solid #ccc !important;
	    border: 0px solid rgba(0,0,0,.15) !important;
	    border-radius: 0px;
	    -webkit-box-shadow: 0 0px 0px rgba(0,0,0,.175) !important; 
	    box-shadow: 0 0px 0px rgba(0,0,0,.175) !important;
	}
	.modal {
	  text-align: center;
	}

	@media screen and (min-width: 768px) { 
	  .modal:before {
	    display: inline-block;
	    
	    content: " ";
	    height: 35%;
	  }
	}

	.modal-dialog {
	  display: inline-block;
	  text-align: left;

	}

	.close {
    	color: #000; 
    	opacity: 1;
	}

</style>

    <header>
		<nav class="navbar navbar-default">
			<div class="container-fluid">				    

				<div class="navbar-header">				
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="template_dev.html">
						<img src="{{ asset('/packages/rent/catalogue/rs-theme/img/logo_small.png') }}" class="logo_small">
					</a>				
				</div>

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">								
						<li>
							<a href="javascript:;" class="rs_btn dashboard">
								<span class="icons icon-dashboard" aria-hidden="true"></span>
								DASHBOARD
							</a>
						</li>
						<li>
							<!--<a href="template_catalogos.html" class="rs_btn catalogos">-->
							<a href="#" class="dropdown-menu rs_btn catalogos" id="rs_submenu_catalogos" data-toggle="dropdown" style="position: initial;border: 0px;">
								<span class="icons icon-catalogos" aria-hidden="true"></span>
								CATÁLOGOS
							</a>
							<div class="dropdown-menu"  role="menu"  aria-labelledby="rs_submenu_catalogos" style="    width: 1024px;position: absolute;margin: 0px;margin-float: 0px !important;height: 65px;border-radius: 0px; left: -266px;"> 
							<!--<ul class="dropdown-menu" role="menu" aria-labelledby="rs_submenu_catalogos">-->
								<ul class="nav navbar-nav " style="margin-left: 146px;">
									<li>							
										<a href="template_dev2.html" class="rs_btn submenu"><span style="padding-left: 15px; ">Inmuebles</span></a>
									</li>						
									<li style="">							
										<a href="javascript:;" class="rs_btn submenu"><span style="padding-left: 15px; ">Clientes</span></a>
									</li>
									<li style="">							
										<a href="javascript:;" class="rs_btn submenu"><span style="padding-left: 15px; ">Contratos</span></a>
									</li>
									<li style="">
										<a href="javascript:;" class="rs_btn submenu"><span style="padding-left: 15px; ">Usuarios</span></a>
									</li>
								</ul>
							<!--</ul>-->
							</div>

						</li>
						<li>							
							<a href="javascript:;" class="rs_btn reportes">
								<span class="icons icon-reportes" aria-hidden="true"></span>
								REPORTES
							</a>
						</li>
						<li>
							<a href="javascript:;" class="rs_btn settings">
								<span class="icons icon-settings" aria-hidden="true"></span>
								SETTINGS
							</a>
						</li>				       
					</ul>
				      
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a id="rs_notifications" href="#" class="dropdown-toggle rs_btn2" data-toggle="dropdown">
								<span class="glyphicon glyphicon-bell" aria-hidden="true">
									<span class="badge badge-notify">4</span>
								</span>
							</a>
																            
							<ul class="dropdown-menu notifications" role="menu" aria-labelledby="rs_notifications">
								<div class="notification-heading">
									<h4 class="menu-title title-notifications">Notificaciones</h4>
									<h4 class="menu-title pull-right">Ver todas<i class="glyphicon glyphicon-circle-arrow-right"></i></h4>
								</div>

								<li class="divider notification-divider"></li>

								<div class="notifications-wrapper">									    
									<a class="content" href="#">
										<div class="notification-item">
											<h4 class="item-title">Notificación de Cobro</h4>
											<p class="item-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
										</div>									      
									</a>

									<a class="content" href="#">
										<div class="notification-item">
											<h4 class="item-title">Notificación de Pago</h4>
											<p class="item-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
										</div>									      
									</a>

									<a class="content" href="#">
										<div class="notification-item">
											<h4 class="item-title">Notificación de Contrato</h4>
											<p class="item-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
										</div>									      
									</a>

									<a class="content" href="#">
										<div class="notification-item">
											<h4 class="item-title">Notificación de Cobro</h4>
											<p class="item-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
										</div>									      
									</a>
								</div>
									
								<li class="divider"></li>
									
								<div class="notification-footer">
									<h4 class="menu-title">Cerrar<i class="glyphicon glyphicon-remove-sign"></i></h4>
								</div>
							</ul>
						</li>
						
						<li>
							<a id="rs_panel_user" href="#" class="dropdown-toggle profile-image" data-toggle="dropdown">
								<img src="{{ asset('/packages/rent/catalogue/rs-theme/img/user.png') }}" class="img-circle pic-user-small"> 
								User <b class="caret"></b>
							</a>

							<ul class="dropdown-menu panel-user" role="menu" aria-labelledby="rs_panel_user" style="right: -1px !important;">

								<div class="panel-user-heading">
									<img src="{{ asset('/packages/rent/catalogue/rs-theme/img/user.png') }}" class="img-circle pic-user-regular">
											
									<div class="panel-user-opcion user-info">
										<h4 class="panel-user-menu-title user-name">
										Nombre Apellido
										</h4>
										<h5 class="panel-user-opcion user-mail">
										ejemplo@ejemplo.com
										</h5>
										<h4 class="panel-user-opcion user-change-info">
											<a href="#">Cambiar mi información</a>
										</h4>
									</div>
								</div>

								<li class="divider panel-user-divider"></li>

								<div class="panel-user-wrapper">								   		

									<div class="panel-user-plazas">
										<h4 class="panel-user-menu-title">Mis plazas</h4>
									</div>
									
									<div class="panel-user-item">
										<div class="row plaza-box-margin-r">
											<div class="col-lg-2 col-md-2 col-sm-2 img-user-large">
												<img src="{{ asset('/packages/rent/catalogue/rs-theme/img/user.png') }} " class="img-circle special-img pic-user-large">
											</div>
											
											<div class="col-lg-8 col-md-8 col-sm-8">
												<h4 class="panel-user-menu-title">Nombre de la Plaza</h4>
												<h5 class="panel-user-opcion plaza-box">
													<span class="label label-success">Actual</span>
												</h5>

											</div>
										</div>
									</div>									      
									
									<a class="content" href="#">
									<div class="panel-user-item">
										<div class="row plaza-box-margin-r">
											<div class="col-lg-2 col-md-2 col-sm-2 img-user-large">
												<img src="{{ asset('/packages/rent/catalogue/rs-theme/img/user.png') }}" class="img-circle special-img pic-user-large">
											</div>
											
											<div class="col-lg-8 col-md-8 col-sm-8">
												<h4 class="panel-user-menu-title">Nombre de la Plaza</h4>
												<h5 class="panel-user-opcion plaza-box">
													<button class="btn btn-primary">Seleccionar</button>
												</h5>

											</div>
										</div>
									</div>
									</a>

									<a class="content" href="#">
									<div class="panel-user-item">
										<div class="row plaza-box-margin-r">
											<div class="col-lg-2 col-md-2 col-sm-2 img-user-large">
												<img src="{{ asset('/packages/rent/catalogue/rs-theme/img/user.png') }}" class="img-circle special-img pic-user-large">
											</div>
											
											<div class="col-lg-8 col-md-8 col-sm-8">
												<h4 class="panel-user-menu-title">Nombre de la Plaza</h4>
												<h5 class="panel-user-opcion plaza-box">
													<button class="btn btn-primary">Seleccionar</button>
												</h5>

											</div>
										</div>
									</div>
									</a>									
								</div>
									    
								<li class="divider"></li>
									    
								<div class="panel-user-footer">
									<h4 class="panel-user-menu-title">Cerrar<i class="glyphicon glyphicon-remove-sign"></i></h4>
								</div>

							</ul>
						   
						</li>				        
						        
					</ul>
				</div>
			</div>
		</nav>

    </header>