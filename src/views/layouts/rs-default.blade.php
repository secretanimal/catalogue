<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">	    
    	@section('title')
            <title>Template :: RentSargent</title>
        @show

    	<!-- Bootstrap -->
        <link rel="stylesheet" href="{{ asset('packages/rent/catalogue/rs-theme/css/bootstrap.min.css') }}">
	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> 
	    <![endif]-->

	    <link rel="stylesheet" type="text/css" href="{{ asset('/packages/rent/catalogue/rs-theme/css/rs-custom-1.0.css') }}" media="screen" />


	    <!-- Lib -->
		<script src="{{ asset('/packages/rent/catalogue/rs-theme/js/jquery-3.1.0.min.js') }}"></script>
		<script src="{{ asset('/packages/rent/catalogue/rs-theme/js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('/packages/rent/catalogue/rs-theme/js/bootstrap-datepicker.js') }}"></script>
		<script src="{{ asset('/packages/rent/catalogue/rs-theme/locales/bootstrap-datepicker.es.min.js') }}" charset="UTF-8"></script>

		<!-- Custom -->
		<script src="{{ asset('/packages/rent/catalogue/rs-theme/js/rs-custom-1.0.js') }}"></script>

	    @yield('head')

  	</head>

  	<body>
		<div id="wrapper">
			
			@yield('header')

			@yield('content') <!--Puede ser dos columnas -->
			
		</div>



		@include('catalogue::layouts.partials.footer')
  	</body>
  </html>