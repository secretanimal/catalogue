@extends('catalogue::layouts.rs-default')


@section('header')
	<!-- Botonera Superior-->
	@include('catalogue::layouts.partials.header-nav-level-1')
@stop

@section('breadcrumbs')
@stop

@section('content')
	<!-- Contenido 2 Columnas -->
	<div id="content" class="panel-2-columns" style="z-index: 0 !important;">
    	<div class="col-lg-3 col-md-2 sidenav">
			Barra Izq
		</div>
		<div class="col-lg-9 col-md-10 text-left">
			<br><br>
			<div class="row">
				<div class="col-lg-12">
					Contenido
				</div>
			</div>
		</div>
	</div>
@show
