@extends('sergeant::layouts.index', ['newTrans' => 'new', 'callback' => 'relatedCustomer'])

@section('head')
    @parent
    <!-- catalogue::customer.index -->
    <script>
        $(document).ready(function() {
            if ($.fn.dataTable)
            {
                $('.datatable-sergeant').dataTable({
                    "displayStart": {{ $offset }},
                    "columnDefs": [
                        @if(isset($modal) && $modal)
                        { "sortable": false, "targets": [8]},
                        { "class": "align-center", "targets": [6,7,8]}
                        @else
                        { "sortable": false, "targets": [8,9]},
                        { "class": "checkbox-column", "targets": [8]},
                        { "class": "align-center", "targets": [6,7,9]}
                        @endif
                    ],
                    'processing': true,
                    'serverSide': true,
                    "ajax": {
                        "url": "{{ route('jsonData' . ucfirst($routeSuffix), ['modal' => $modal? 1 : 0]) }}",
                        "type": "POST",
                        "headers": {
                            "X-CSRF-TOKEN": "{{ csrf_token() }}"
                        }
                    }
                }).fnSetFilteringDelay();
            }
        });
    </script>
    <!-- /.catalogue::customer.index -->
@stop

@section('tHead')
    <!-- catalogue::customer.index -->
    <tr>
        <th data-hide="phone,tablet">ID.</th>
        <th data-hide="phone">{{ trans_choice('sergeant::sergeant.company', 1) }}</th>
        <th data-class="expand">{{ trans('sergeant::sergeant.name') }}</th>
        <th data-hide="phone">{{ trans('sergeant::sergeant.surname') }}</th>
        <th data-hide="phone">{{ trans('sergeant::sergeant.email') }}</th>
        <th data-hide="phone">{{ trans_choice('sergeant::sergeant.group', 1) }}</th>
        <th data-hide="phone,tablet">{{ trans('sergeant::sergeant.active') }}</th>
        <th data-hide="phone,tablet">{{ trans('sergeant::sergeant.confirmed') }}</th>
        @if(! isset($modal) || isset($modal) && !$modal)
            <th class="checkbox-column"><input type="checkbox" class="uniform"></th>
        @endif
        <th>{{ trans_choice('sergeant::sergeant.action', 2) }}</th>
    </tr>
    <!-- /.catalogue::customer.index -->
@stop