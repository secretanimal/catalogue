<li{!! is_current_resource(['catalogue-group', 'catalogue-customer']) !!}>
    <a href="javascript:void(0)"><i class="fa fa-users"></i>{{ trans('catalogue::sergeant.package_name') }}</a>
    <ul class="sub-menu">
        @if(is_allowed('catalogue-customer', 'access'))
            <li{!! is_current_resource('catalogue-customer') !!}><a href="{{ route('catalogueCustomer', ['modal' => 0]) }}"><i class="fa fa-user"></i>{{ trans_choice('sergeant::sergeant.customer', 2) }}</a></li>
        @endif
        @if(is_allowed('catalogue-group', 'access'))
            <li{!! is_current_resource('catalogue-group') !!}><a href="{{ route('catalogueGroup') }}"><i class="fa fa-users"></i>{{ trans_choice('sergeant::sergeant.group', 2) }}</a></li>
        @endif
    </ul>
</li>