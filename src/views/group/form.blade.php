@extends('sergeant::layouts.form')

@section('rows')
    <!-- catalogue::groups.form -->
    @include('sergeant::includes.html.form_text_group', [
        'label' => 'ID',
        'name' => 'id',
        'value' => old('id', isset($object)? $object->id_300 : null),
        'readOnly' => true,
        'fieldSize' => 2
    ])
    @include('sergeant::includes.html.form_text_group', [
        'label' => trans('sergeant::sergeant.name'),
        'name' => 'name',
        'value' => old('name', isset($object)? $object->name_300 : null),
        'maxLength' => '255',
        'rangeLength' => '2,255',
        'required' => true]
    )
    <!-- /.catalogue::groups.form -->
@stop