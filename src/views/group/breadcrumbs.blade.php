<!-- catalogue::groups.breadcrumbs -->
<li>
    <a href="javascript:void(0)">{{ trans('catalogue::sergeant.package_name') }}</a>
</li>
<li class="current">
    <a href="{{ route($routeSuffix) }}">{{ trans_choice($objectTrans, 2) }}</a>
</li>
<!-- /.catalogue::groups.breadcrumbs -->